class Shape {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    perimeter() {
        return undefined;
    }

    area() {
        return undefined;
    }

}

class Rectangle extends Shape {

    constructor(x, y) {
        super(x, y);
    }

    perimeter() {
        return 2 * this.x + 2 * this.y;
    }

    area() {
        return this.x * this.y;
    }
}

class Square extends Shape {

    constructor(x) {
        super(x, null);
    }

    perimeter() {
        return 4 * this.x;
    }

    area() {
        return this.x * this.x;
    }
}

const rectangle = new Rectangle(2, 3);
console.log("Perimeter of rectangle: " + rectangle.perimeter());
console.log("Area of rectangle: " + rectangle.area());

const square = new Square(3);
console.log("Perimeter of square: " + square.perimeter());
console.log("Area of square: " + square.area());