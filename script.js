let one = "";
const two = "Two";
let three = 2.567;
const four = 15;
let five = true;
let six = false;
const seven = null;
let eight;

console.log("Section 1")
console.log("1" + "-" + one);
console.log("2" + "-" + two);
console.log("3" + "-" + three);
console.log("4" + "-" + four);
console.log("5" + "-" + five);
console.log("6" + "-" + six);
console.log("7" + "-" + seven);
console.log("8" + "-" + eight);

console.log("Section 2")
console.log(typeof(one));
console.log(typeof(two));
console.log(typeof(three));
console.log(typeof(four));
console.log(typeof(five));
console.log(typeof(six));
console.log(typeof(seven));
console.log(typeof(eight));

console.log("Section 3")
const resultOfSum = (three + four);
const resultOfSubtraction = (three - four);
const resultOfMultiplication = (three * four);
const resultOfDivision = (three / four);

console.log(resultOfSum);
console.log(resultOfSubtraction);
console.log(resultOfMultiplication);
console.log(resultOfDivision);

console.log("Section 4");
one = "Galatasaray is my favourite club";
console.log(`${one}` + " " + `${two}`);

console.log("Section 5");
let newThree = String(three);
console.log(newThree);
let newFour = String(four);
console.log(newFour);

if (seven == eight ){
    console.log(seven)
} else if (seven === eight) {
    console.log (eight)
}


